#!/usr/bin/python
#title           :birthday.py
#description     :This will send email when it is people their birthday
#author          :Berend de Groot
#date            :23-05-2012
#version         :4.0
#usage           :python birthday.py
#notes           :Inculdes 2 other files: birthday.txt & birthday.cfg. Won't work withouth them.
#python_version  :2.7.3  
#==============================================================================

# Import functions
import datetime
import linecache
import smtplib
import ConfigParser

# Start Functions
def settings():
	# Read the message, server, senders email from the config file.
	cp = ConfigParser.ConfigParser()
	cp.read("/home/birthday.cfg")
	msg = eval(cp.get("settings", "message"))
	server = eval(cp.get("settings", "server"))
	send = eval(cp.get("settings", "send"))
	return msg, server, send

def sendmail(server,rec,send,msg):
	#sends the email to the person. server = server. rec = who gets the mail, send = sender, msg = message
	smtp = smtplib.SMTP(server)
	smtp.sendmail(send, rec, msg)	
	return
def changename(rec, msg):
	# Change the word "name" in the message with the specific name
	name = rec.split(".")
	msg = msg.replace("name", name[0])
	return msg
# End Functions

# Sets today's date
today = datetime.date.today()
date = today.strftime("%m/%d")

# counter & timer. 0 - 43 (depans on the file)
c = 0
t = len(open("/home/birthday.txt").readlines())
while (c < t):
	c = c+1
	# Get the date from the file
	f = file("/home/birthday.txt","r")
	line = linecache.getline("/home/birthday.txt", c).rstrip("\n")
	info = line.split(" ")
	# Check if any date is equal to today's date
	if date == info[0]:
		# Use functions te correct way
		rec = info[1]
		msg,server,send = settings()
		msg = changename(rec,msg)
		sendmail(server,rec,send,msg)
print 'done'
#Let me know the code is done